package com.example.ale.englishexam;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class MyArrayAdapterDBShow extends ArrayAdapter {

    private Object[] object;

    public MyArrayAdapterDBShow(@NonNull Context context, int resource, @NonNull Object[] objects) {
        super(context, resource, objects);
        this.object = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater li = LayoutInflater.from(getContext());
        View v = li.inflate(R.layout.my_item_db, parent, false);

        Definition definition = (Definition) object[position];

        TextView tv_word = v.findViewById(R.id.word);
        TextView tv_def = v.findViewById(R.id.definition);

        tv_word.setText(definition.getWord()+" :");
        tv_def.setText(definition.getDefinition());

        return v;
    }
}
