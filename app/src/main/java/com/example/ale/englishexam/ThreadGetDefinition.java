package com.example.ale.englishexam;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.TextView;

import java.util.ArrayList;

public class ThreadGetDefinition extends Thread {

    private int number_definition;
    private ArrayList listDef;
    private SQLiteDatabase db;
    private Activity activity;
    private Definition def;
    private TextView textView;

    public ThreadGetDefinition(int number_definition, Definition definition, ArrayList listDef, SQLiteDatabase db, Activity activity, TextView textView){
        this.number_definition = number_definition;
        this.listDef = listDef;
        this.db = db;
        this.activity = activity;
        this.def = definition;
        this.textView = textView;
    }

    @Override
    public void run() {
        int tot = number_definition;
        Log.d("PJDM","num def: "+number_definition);
        // scelgo id;
        int attempt = tot - 1;
        int position = (int) Math.round(Math.random()*(tot-1));

        Log.d("PJDM","position random:"+position);
        int index = listDef.indexOf(position);
        while (index != -1){
            position = (int) Math.round(Math.random()*(tot-1));
            index = listDef.indexOf(position);
            attempt--;
            // se #tentativi >= #elementi => smetto iterare e prendo def già capitata
            if (attempt <= 0){
                break;
            }
        }
        listDef.add(position);
        Log.d("PJDM","position fne while: "+position);
        // facciamo una query
        // String selection = MyDictionaryDB.MyDictionaryEntry._ID+" = ?";
        // String[] arg_selection = {id+""};

        // per motivi di sicurezza contro attacchi non si passa un'unica stringa ma più argomenti
        Cursor cursor = db.query(MyDictionaryDB.MyDictionaryEntry.TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                null);
        cursor.moveToPosition(position);
        // cursor.moveToPosition(id-1);
        final String definition = cursor.getString(1);
        def.setDefinition(definition);
        String word = cursor.getString(2);
        def.setWord(word);
        Log.d("PJDM","word in thread: "+def.getWord());

        cursor.close();

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.d("PJDM","sono entrato in Runnable()");
                textView.setText(definition);
            }
        });
    }
}
