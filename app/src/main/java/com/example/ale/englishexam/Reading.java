package com.example.ale.englishexam;

import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class Reading extends AppCompatActivity {

    private Timer timer;

    private ParseJson json_parse;

    private int points_part1 = 0;
    private int points_part2 = 0;
    private int points_part3 = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reading);

        // Timer
        TextView tw_timer = findViewById(R.id.timer);
        Button btt_startPause = findViewById(R.id.startPause);
        long timeDuration = 3600000; // 1 ora
        timer = new Timer(timeDuration, tw_timer, btt_startPause,this);

        // PARTE TIMER
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey("nome_file")){
            String nome_file = extras.getString("nome_file");

            ParseJsonAT parseJsonAT = new ParseJsonAT();
            parseJsonAT.execute(nome_file);

        }
    }

    @Override
    protected void onPause() {
        if (timer.ismTimerRunning()){
            timer.pauseTimer();
        }
        super.onPause();
    }

    // se si preme indietro fermo timer
    @Override
    public void onBackPressed() {
        if (timer.ismTimerRunning()){
            timer.pauseTimer();
        }
        super.onBackPressed();
    }

    // PARTE TIMER
    public void startPause(View view) {
        // se il tempo scorre
        if (timer.ismTimerRunning()) {
            timer.pauseTimer();
        } else {
            timer.startTimer();
        }

    }

    private int getId(String name){
        int id = 0;
        try {
            id = R.id.class.getField(name).getInt(null);
        } catch (IllegalAccessException | NoSuchFieldException e) {
            e.printStackTrace();
        }
        return id;
    }

    // RIEMPI PARTE1
    private void RiempiDomandeParte1(ParseJson json_parse) {
        json_parse.parseParte1_reading();
        String[][] domandeparte1 = json_parse.getDomandeParte1();

        String testo = json_parse.getTesto_first_part();
        insertText(R.id.longtext_part1, testo);

        int domande = 8;
        int possibili_risposte = 4;
        String name = "parte1_domanda";
        for (int i = 0; i < domande; i++){
            int index = i + 1;
            int id = getId(name+index);
            // inserisco domanda
            insertText(id,domandeparte1[i][0]);
            for (int j = 0; j < possibili_risposte; j++){
                // inserisco possibili risposte
                String name_rb = "rbparte1_d";
                int index2 = j+1;
                int id_rb = getId(name_rb+index+"_"+index2);
                insertRadioBotton(id_rb,domandeparte1[i][j+1]);

            }
        }
    }

    private void insertText(int id, String question){
        TextView textView = findViewById(id);
        textView.setText(question);
    }

    private void insertRadioBotton(int id, String question){
        RadioButton radioButton = findViewById(id);
        radioButton.setText(question);
    }

    private void riempiSpinner(int id, String[] choices){
        Spinner spinner = findViewById(id);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,choices);
        spinner.setAdapter(arrayAdapter);
    }

    private void riempiSpinner_mylayout(int id, String[] choices){
        Spinner spinner = findViewById(id);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this,R.layout.spinner_element,choices);
        spinner.setAdapter(arrayAdapter);
    }

    private void RiempiDomandeParte3(ParseJson json_parse) {
        json_parse.parseParte3_reading();
        String[][] domandeparte3 = json_parse.getDomandeParte3();

        String[][] titolo_testo = json_parse.getTitle_test_part3();

        insertText(R.id.parte3_titleA, titolo_testo[0][0]);
        insertText(R.id.parte3_textA, titolo_testo[0][1]);

        insertText(R.id.parte3_titleB, titolo_testo[1][0]);
        insertText(R.id.parte3_textB, titolo_testo[1][1]);

        insertText(R.id.parte3_titleC, titolo_testo[2][0]);
        insertText(R.id.parte3_textC, titolo_testo[2][1]);

        insertText(R.id.parte3_titleD, titolo_testo[3][0]);
        insertText(R.id.parte3_textD, titolo_testo[3][1]);

        // parte domande nuova:
        int domande = 15;
        String name = "parte3_domanda";
        String name_spinner = "parte3_spinner";
        String[] choices = {"A","B","C","D"};
        for (int i = 0; i < domande; i++){
            int index = i + 1;
            // inserisco domanda
            int id = getId(name+index);
            insertText(id,domandeparte3[i][0]);
            // spinner
            int id_spinner = getId(name_spinner+index);
            riempiSpinner(id_spinner,choices);
        }
    }

    public void RiempiDomandeParte2(ParseJson json_parse){
        json_parse.parseParte2_reading();
        String[][] domandeparte2 = json_parse.getDomandeParte2();

        String testo = json_parse.getTesto_second_part();
        insertText(R.id.parte2_longtext, testo);

        // parte domande nuova:
        int domande = 8;
        String name = "parte2_domanda";
        for (int i = 0; i < domande; i++){
            int index = i + 1;
            // inserisco domanda
            int id = getId(name+index);
            insertText(id,domandeparte2[i][0]);
        }

        String name_spinner = "parte2_spinner";
        String[] choices = {"A","B","C","D","E","F","G","H"};
        for (int i = 0; i < domande -1; i++){
            // spinner
            int index = i + 1;
            int id_spinner = getId(name_spinner+index);
            Log.d("PJDM","id: "+id_spinner);
            Log.d("PJDM","name: "+name_spinner+index);
            riempiSpinner_mylayout(id_spinner,choices);
        }

    }

    /* ------------------------------- CORREZZIONE -----------------------------------------------*/

    public void correct_test(View view){
        CorrectPart1();
        correctPart2();
        correctPart3();

        // rendo invisible il bottone correggi
        Button btt_end = findViewById(R.id.bttend);
        btt_end.setClickable(false);
        //btt_end.setVisibility(View.INVISIBLE);
    }

    // CORREZIONE PARTE 1

    public void CorrectPart1(){
        final String[] risposteparte1 = json_parse.getRisposteParte1();

        int domande = 8;
        String name = "rgparte1_";
        for (int i = 0; i < domande; i++){
            int index = i + 1;
            // inserisco domanda
            int id = getId(name+index);
            CorrectPart1_domande(id, risposteparte1[i]);
        }

        TextView tw = findViewById(R.id.parte1_punti);
        String ris = "First part points: "+points_part1+"/8";
        tw.setText(ris);

    }

    public void CorrectPart1_domande(int rg_id, String answer){
        RadioGroup rg = findViewById(rg_id);

        int id = rg.getCheckedRadioButtonId();
        if (id != -1){
            RadioButton rb = findViewById(id);
            String attempt = rb.getText().toString();

            if (attempt.compareTo(answer) == 0){
                points_part1++;
                rb.setBackgroundColor(Color.GREEN);
            }else{
                rb.setBackgroundColor(Color.RED);
                correctPart1_radioButton(rg, answer);

            }

        }else{
            correctPart1_radioButton(rg, answer);
        }

    }

    public void correctPart1_radioButton(RadioGroup rg, String answer){
        RadioButton rb;
        String attempt;
        int count = rg.getChildCount();
        ArrayList<RadioButton> listOfRadioButtons = new ArrayList<>();
        for (int i=0;i<count;i++) {
            View o = rg.getChildAt(i);

            if (o instanceof RadioButton) {
                listOfRadioButtons.add((RadioButton)o);
            }
        }

        for (int i=0; i<listOfRadioButtons.size(); i++ ){
            rb = listOfRadioButtons.get(i);
            attempt = rb.getText().toString();
            if (attempt.compareTo(answer) == 0) {
                rb.setBackgroundColor(Color.GREEN);
            }
        }

    }

    // CORREZIONE PARTE 2

    public void correctPart2(){
        final String[] risposteparte2 = json_parse.getRisposteParte2();

        int domande = 7;
        String name = "parte2_spinner";
        for (int i = 0; i < domande; i++){
            int index = i + 1;
            // inserisco domanda
            int id = getId(name+index);
            CorrectPart2_domande(id, risposteparte2[i]);
        }

        TextView tw = findViewById(R.id.parte2_punti);
        String ris = "Second part points: "+points_part2+"/7";
        tw.setText(ris);


    }

    public void CorrectPart2_domande(int tw_id, String answer){
        Spinner s = findViewById(tw_id);
        String attempt = s.getSelectedItem().toString().toLowerCase().trim();

        if (attempt.compareTo(answer.toLowerCase().trim()) == 0){
            points_part2++;
            s.setBackgroundColor(Color.GREEN);
        }else{
            int position = answer.charAt(0) - 65; // nota A = 65
            s.setSelection(position);
            s.setBackgroundColor(Color.RED);
        }
    }

    // CORREZIONE PARTE 3

    public void correctPart3(){

        final String[] risposteparte3 = json_parse.getRisposteParte3();

        int domande = 15;
        String name = "parte3_spinner";
        for (int i = 0; i < domande; i++){
            int index = i + 1;
            // inserisco domanda
            int id = getId(name+index);
            CorrectPart3_domande(id, risposteparte3[i]);
        }

        TextView tw = findViewById(R.id.parte3_punti);
        String ris = "Second part points: "+points_part3+"/15";
        tw.setText(ris);

        tw = findViewById(R.id.tot_punti);
        int tot = points_part1 + points_part2 +points_part3;
        String tot_string = "Overall points: "+tot+"/30";
        tw.setText(tot_string);
    }

    public void CorrectPart3_domande(int tw_id, String answer){
        Spinner s = findViewById(tw_id);
        String attempt = s.getSelectedItem().toString().toLowerCase().trim();

        if (attempt.compareTo(answer.toLowerCase().trim()) == 0){
            points_part3++;
            s.setBackgroundColor(Color.GREEN);
        }else{
            int position = answer.charAt(0) - 65; // nota A = 65
            s.setSelection(position);
            s.setBackgroundColor(Color.RED);
        }
    }

    // Asyntask per parsing file json
    private class ParseJsonAT extends AsyncTask<String, Void, ParseJson> {

        @Override
        protected ParseJson doInBackground(String... s) {
            String nome_file = s[0];

            // faccio parsing del file json
            Log.d("PJDM","nome file: "+nome_file);
            int id = Reading.this.getResources().getIdentifier(nome_file,"raw",Reading.this.getPackageName());
            InputStream is = Reading.this.getResources().openRawResource(id);
            json_parse = new ParseJson(is);

            // chiudo file
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return json_parse;
        }

        @Override
        protected void onPostExecute(ParseJson json) {
            // "titolo"
            String titolo = "Reading " + json_parse.getLevel()+" - "+json_parse.getSeries();
            TextView tw = findViewById(R.id.textView3);
            tw.setText(titolo);

            // INSERISCO I DATI - PARTE1 nota: domandaParte1 = [domandax][domanda,s1,s2,s3]
            RiempiDomandeParte1(json);

            // INSERISCO I DATI - PARTE2 nota: domandaParte3 = [domandax][domandax]
            RiempiDomandeParte2(json);

            // INSERISCO I DATI - PARTE3 nota: domandaParte2 = [domandax][domandax-1,risposta-x,domandax-2]
            RiempiDomandeParte3(json);

            // inserisco titoli
            insertText(R.id.title_first_part, json_parse.getTitle_first_part());
            insertText(R.id.title_second_part, json_parse.getTitle_second_part());
            insertText(R.id.title_third_part, json_parse.getTitle_third_part());

        }
    }

}


