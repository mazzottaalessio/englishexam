package com.example.ale.englishexam;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.lang.reflect.Field;

public class exam_list extends AppCompatActivity {

    private String type = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exam_list);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            type = extras.getString("type");
        }

        // titolo pagina
        TextView tw = findViewById(R.id.title);
        if (type.compareTo("listening") == 0){
            tw.setText(R.string.listening_list);
        }else if (type.compareTo("reading") == 0){
            tw.setText(R.string.reading_list);
        }else if (type.compareTo("useofenglish") == 0){
            tw.setText(R.string.uoe_list);
        }else if (type.compareTo("speaking") == 0){
            tw.setText(R.string.speaking_list);
        }else if (type.compareTo("writing") == 0){
            tw.setText(R.string.writing_list);
        }

        final String[] exam_list = examRaw(type);

        // creo array di file (a seconda del tipo scelto)

        String[] exam_title_list = getTitles(exam_list,type);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(
                this,R.layout.my_item_listarray,exam_title_list);

        // associo array adapter a list view
        ListView listView = findViewById(R.id.lw1);
        listView.setAdapter(arrayAdapter);

        // creo Listener su evento di click su elemento della lista
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent detailIntent = new Intent();
                if (type.compareTo("listening") == 0){
                    detailIntent.setClass(exam_list.this, Listening.class);
                }else if(type.compareTo("reading") == 0){
                    detailIntent.setClass(exam_list.this, Reading.class);
                }else if(type.compareTo("speaking") == 0){
                    detailIntent.setClass(exam_list.this, Speaking.class);
                }else if(type.compareTo("writing") == 0){
                    detailIntent.setClass(exam_list.this, Writing.class);
                }else if(type.compareTo("useofenglish") == 0){
                    detailIntent.setClass(exam_list.this, UseOfEnglishActivity.class);
                }

                // Boundle, gli passiamo il nome del file
                detailIntent.putExtra("nome_file",exam_list[position]);
                startActivity(detailIntent);
            }
        });

    }

    public String[] examRaw(String type){
        Field[] fields=R.raw.class.getFields();
        String[] file = new String[fields.length];
        int indice_file = 0;

        // se il nome contiene type lo aggiungo all'elenco
        for(int count=0; count < fields.length; count++){
            String name = fields[count].getName();
            if (name.contains(type)){
                if (name.contains("json")){
                    file[indice_file] = name;
                    indice_file++;
                }

            }

        }

        // ritorno array con il giusto numero di elementi
        String[] name_array = new String[indice_file];
        for(int i=0; i < indice_file; i++){
            name_array[i] = file[i];
        }

        return name_array;
    }

    public String[] getTitles(String[] list, String type){
        int len = list.length;
        String[] title_list = new String[len];
        String titolo;
        int pos = type.length() + 1;
        for (int i = 0; i < len; i++) {

            if (type.compareTo("useofenglish") == 0) {
                String type_uoe = "use of english";
                titolo = type_uoe + " " + list[i].substring(pos, pos + 2) + " " + list[i].substring(pos + 3, pos + 5);
                title_list[i] = titolo.toUpperCase();
            } else {
                titolo = type + " " + list[i].substring(pos, pos + 2) + " " + list[i].substring(pos + 3, pos + 5);
                title_list[i] = titolo.toUpperCase();
            }

        }
        return title_list;
    }

}
