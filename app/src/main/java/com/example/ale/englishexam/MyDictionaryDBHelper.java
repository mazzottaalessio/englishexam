package com.example.ale.englishexam;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MyDictionaryDBHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "myDictionary.db";

    public MyDictionaryDBHelper(Context context){
        super(context ,DATABASE_NAME,null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // creare DB
        String SQL_CREATE_ENTRIES = String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY, %s TEXT, %s TEXT)",
                MyDictionaryDB.MyDictionaryEntry.TABLE_NAME,
                MyDictionaryDB.MyDictionaryEntry._ID,
                MyDictionaryDB.MyDictionaryEntry.COLUMN_NAME_DEF,
                MyDictionaryDB.MyDictionaryEntry.COLUMN_NAME_WORD);
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
