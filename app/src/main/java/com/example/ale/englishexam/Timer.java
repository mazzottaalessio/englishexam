package com.example.ale.englishexam;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

public class Timer {
    private TextView mTextViewCountDown;
    private Button mButtonStartPause;
    private CountDownTimer mCountDownTimer;
    private long timeDuration;
    private Context context;

    private MediaPlayer mediaPlayer;

    private boolean end;

    private boolean mTimerRunning; // indica se il tempo sta scorrendo o meno

    private long mTimeLeftInMillis;

    public boolean isEnd() {return end;}

    public void setEnd(boolean end) {this.end = end;}

    public boolean ismTimerRunning() {
        return mTimerRunning;
    }

    public void ismTimerRunning(boolean newTimeRunning) {
        this.mTimerRunning = newTimeRunning;
    }

    public void setmTimeLeftInMillis(long mTimeLeftInMillis) {
        this.mTimeLeftInMillis = mTimeLeftInMillis;
    }

    public Timer(long timeDuration, TextView timer, Button startPause, Context ctx){
        this.end = false;
        this.mTimerRunning = false;
        this.timeDuration = timeDuration;
        this.mTimeLeftInMillis = timeDuration;
        this.mTextViewCountDown = timer;
        this.mButtonStartPause = startPause;
        this.context = ctx;
    }

    // in pausa fermo il timer
    public void pauseTimer() {
        mCountDownTimer.cancel(); // ferma il tempo (IMPORTANTE!)
        mTimerRunning = false;
        mButtonStartPause.setText(R.string.butt_timer);
    }

    public void azzeraTimer(){
        mTextViewCountDown.setText(R.string.timer);
    }

    public void startTimer() {
        this.end = false;
        // parametri sono: secondi rimasti in ms e intervallo in ms
        mCountDownTimer = new CountDownTimer(mTimeLeftInMillis, 1000) {
            @Override
            // ad ogni intervallo aggiorno il valore del tempo rimasto e il valore da vedere nell'App
            public void onTick(long millisUntilFinished) {
                mTimeLeftInMillis = millisUntilFinished;
                updateCountDownText();
            }

            // al termine del timer, faccio in modo che sia possibile resettarlo
            @Override
            public void onFinish() {

                mTimerRunning = false;
                mButtonStartPause.setText(("Restart"));

                updateCountDownText();
                mTimeLeftInMillis = timeDuration;

                // rendere invisibile il button start/pause
                //mButtonStartPause.setVisibility(View.INVISIBLE);

                // scrivo 00:00 sul timer
                azzeraTimer();
                //mTextViewCountDown.setText("00:00");

                // Toast (tempo finito)
                Toast.makeText(context,"Timeout!", Toast.LENGTH_SHORT).show();

                // end = true
                end = true;

                // suono finale:
                mediaPlayer = MediaPlayer.create(context, R.raw.end);
                mediaPlayer.start();
            }
        }.start();

        mTimerRunning = true;
        mButtonStartPause.setText(R.string.butt_pause);

    }


    private void updateCountDownText() {
        int minutes = (int) mTimeLeftInMillis / 1000 / 60;
        int seconds = (int) (mTimeLeftInMillis / 1000) % 60;
        String timeLeftFormatted = String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds);

        mTextViewCountDown.setText(timeLeftFormatted);
    }
}
