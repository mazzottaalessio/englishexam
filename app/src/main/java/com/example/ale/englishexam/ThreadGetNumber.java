package com.example.ale.englishexam;

import android.app.Activity;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class ThreadGetNumber extends Thread {

    private Definition def;
    private Activity activity;
    private Timer timer;
    private int duration;
    private TextView tw_timer;
    private String IP = "http://192.168.178.46:8080/ProvaServlet/GetNumber";



    public ThreadGetNumber(Definition def, Activity activity, Timer timer, int duration, TextView tw){

        this.def = def;
        this.activity = activity;
        this.timer = timer;
        this.duration = duration;
        this.tw_timer = tw;
    }

    public void run(){
        try{
            URL url = new URL(IP);

            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream is = urlConnection.getInputStream();
            Log.d("PJDM",is+"");

            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String String_number = "";
            String line = reader.readLine();
            Log.d("PJDM","line nel while: "+line);
            while(line != null){
                Log.d("PJDM","line nel while: "+line);
                String_number+=line;
                line = reader.readLine();
            }
            def.setNum_element(Integer.parseInt(String_number));
            urlConnection.disconnect();
            is.close();
            reader.close();

        } catch (IOException e) {
            // -1 indica un errore
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(activity.getBaseContext(),"Connection problem, please check your connection",Toast.LENGTH_SHORT).show();

                    // fermo il timer
                    if (timer.ismTimerRunning()){
                        timer.pauseTimer();
                        timer.setEnd(true);
                        timer.setmTimeLeftInMillis(duration);
                        tw_timer.setText(R.string.timer);
                    }
                }
            });
            def.setNum_element(-1);
            e.printStackTrace();
        }
    }

}

