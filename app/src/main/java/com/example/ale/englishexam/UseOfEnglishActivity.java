package com.example.ale.englishexam;

import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class UseOfEnglishActivity extends AppCompatActivity {

    private Timer timer;

    private ParseJson json_parse;

    private int points_part1 = 0;
    private int points_part2 = 0;
    private int points_part3 = 0;
    private int points_part4 = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_use_of_english);

        // Timer
        TextView tw_timer = findViewById(R.id.timer);
        Button btt_startPause = findViewById(R.id.startPause);
        long timeDuration = 60*45*1000; // 45 minuti
        timer = new Timer(timeDuration, tw_timer, btt_startPause,this);

        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey("nome_file")){
            String nome_file = extras.getString("nome_file");

            ParseJsonAT parseJsonAT = new ParseJsonAT();
            parseJsonAT.execute(nome_file);

        }
    }

    // se si preme indietro fermo timer
    @Override
    public void onBackPressed() {
        if (timer.ismTimerRunning()){
            timer.pauseTimer();
        }
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        if (timer.ismTimerRunning()){
            timer.pauseTimer();
        }
        super.onPause();
    }


    // PARTE TIMER
    public void startPause(View view) {
        // se il tempo scorre
        if (timer.ismTimerRunning()) {
            timer.pauseTimer();
        } else {
            timer.startTimer();
        }

    }

    private int getId(String name){
        int id = 0;
        try {
            id = R.id.class.getField(name).getInt(null);
        } catch (IllegalAccessException | NoSuchFieldException e) {
            e.printStackTrace();
        }
        return id;
    }

    // RIEMPI PARTE 1
    private void RiempiDomandeParte1(ParseJson json_parse) {
        json_parse.parseParte1_useofenglish();
        String[][] domandeparte1 = json_parse.getDomandeParte1();

        String testo = json_parse.getTesto_first_part();
        insertText(R.id.longtext_part1, testo);

        int domande = 12;
        int possibili_risposte = 4;
        String name = "parte1_domanda";
        for (int i = 0; i < domande; i++){
            int index = i + 1;
            int id = getId(name+index);
            // inserisco domanda
            insertText(id,domandeparte1[i][0]);
            for (int j = 0; j < possibili_risposte; j++){
                // inserisco possibili risposte
                String name_rb = "rbparte1_d";
                int index2 = j+1;
                int id_rb = getId(name_rb+index+"_"+index2);
                insertRadioBotton(id_rb,domandeparte1[i][j+1]);

            }
        }
    }

    private void insertText(int id, String question){
        TextView textView = findViewById(id);
        textView.setText(question);
    }

    private void insertRadioBotton(int id, String question){
        RadioButton radioButton = findViewById(id);
        radioButton.setText(question);
    }

    public void RiempiDomandeParte2(ParseJson json_parse) {
        json_parse.parseParte2_useofenglish();

        String testo = json_parse.getTesto_second_part();
        insertText(R.id.parte2_longtext, testo);

    }
    private void RiempiDomandeParte3(ParseJson json_parse) {
        json_parse.parseParte3_useofenglish();
        String[][] domandeparte3 = json_parse.getDomandeParte3();

        String testo = json_parse.getTesto_third_part();
        insertText(R.id.parte3_longtext, testo);

        int domande = 10;
        String name = "parte3_domanda";
        for (int i = 0; i < domande; i++){
            int index = i + 1;
            int id = getId(name+index);
            // inserisco domanda
            insertText(id,domandeparte3[i][0]);
        }

    }

    private void RiempiDomandeParte4(ParseJson json_parse) {
        json_parse.parseParte4_useofenglish();
        String[][] domandeparte4 = json_parse.getDomandeParte4();

        int domande = 8;
        String name = "parte4_domanda";
        for (int i = 0; i < domande; i++){
            int index = i + 1;
            int id = getId(name+index);
            // inserisco domanda
            insertText(id,domandeparte4[i][0]);
            int id2 = getId(name+index+"_2");
            insertText(id2, domandeparte4[i][1]);
        }
    }



    /* ------------------------ CORREZIONE ------------------------ */

    public void correct_test(View view) {
        CorrectPart1();
        correctPart2();
        correctPart3();
        correctPart4();

        // rendo invisible il bottone correggi
        Button btt_end = findViewById(R.id.bttend);
        btt_end.setClickable(false);
        //btt_end.setVisibility(View.INVISIBLE);
    }


    // CORREZIONE PARTE 1

    private void CorrectPart1() {
        String[] risposte = json_parse.getRisposteParte1();

        int domande = 12;
        String name = "rgparte1_";
        for (int i = 0; i < domande; i++){
            int index = i + 1;
            int id = getId(name+index);
            // inserisco domanda
            CorrectPart1_domande(id, risposte[i]);
        }

        TextView tw = findViewById(R.id.parte1_punti);
        String ris = "First part points: "+points_part1+"/12";
        tw.setText(ris);
    }

    public void CorrectPart1_domande(int rg_id, String answer){
        RadioGroup rg = findViewById(rg_id);

        int id = rg.getCheckedRadioButtonId();
        if (id != -1){
            RadioButton rb = findViewById(id);
            String attempt = rb.getText().toString();

            if (attempt.compareTo(answer) == 0){
                points_part1++;
                rb.setBackgroundColor(Color.GREEN);
            }else{
                rb.setBackgroundColor(Color.RED);
                correctPart1_radioButton(rg, answer);

            }

        }else{
            correctPart1_radioButton(rg, answer);
        }

    }

    public void correctPart1_radioButton(RadioGroup rg, String answer){
        RadioButton rb;
        String attempt;
        int count = rg.getChildCount();
        ArrayList<RadioButton> listOfRadioButtons = new ArrayList<>();
        for (int i=0;i<count;i++) {
            View o = rg.getChildAt(i);

            if (o instanceof RadioButton) {
                listOfRadioButtons.add((RadioButton)o);
            }
        }

        for (int i=0; i<listOfRadioButtons.size(); i++ ){
            rb = listOfRadioButtons.get(i);
            attempt = rb.getText().toString();
            if (attempt.compareTo(answer) == 0) {
                rb.setBackgroundColor(Color.GREEN);
            }
        }

    }

    // CORREZIONE PARTE 2

    private void correctPart2() {
        String[] risposte = json_parse.getRisposteParte2();

        int domande = 12;
        String name = "parte2_risposta";
        for (int i = 0; i < domande; i++){
            int index = i + 1;
            int id = getId(name+index);
            // inserisco domanda
            CorrectPart2_domande(id, risposte[i]);
        }

        TextView tw = findViewById(R.id.parte2_punti);
        String ris = "Second part points: "+points_part2+"/12";
        tw.setText(ris);

    }

    public void CorrectPart2_domande(int id, String answer){
        EditText editText = findViewById(id);
        String attempt = editText.getText().toString();

        if (attempt.trim().toUpperCase().compareTo(answer.trim().toUpperCase()) == 0){
            points_part2++;
            editText.setTextColor(Color.GREEN);
        }else{
            editText.setText(answer);
            editText.setTextColor(Color.RED);
        }

    }

    // PARTE 3
    private void correctPart3() {
        String[] risposte = json_parse.getRisposteParte3();

        int domande = 10;
        String name = "parte3_risposta";
        for (int i = 0; i < domande; i++){
            int index = i + 1;
            int id = getId(name+index);
            // inserisco domanda
            CorrectPart3_domande(id, risposte[i]);
        }

        TextView tw = findViewById(R.id.parte3_punti);
        String ris = "Third part points: "+points_part3+"/10";
        tw.setText(ris);

    }

    public void CorrectPart3_domande(int id, String answer){
        EditText editText = findViewById(id);
        String attempt = editText.getText().toString();

        if (attempt.trim().toUpperCase().compareTo(answer.trim().toUpperCase()) == 0){
            points_part3++;
            editText.setTextColor(Color.GREEN);
        }else{
            editText.setText(answer);
            editText.setTextColor(Color.RED);
        }

    }

    // PARTE 4

    private void correctPart4() {
        String[] risposte = json_parse.getRisposteParte4();

        int domande = 8;
        String name = "parte4_risposta";
        for (int i = 0; i < domande; i++){
            int index = i + 1;
            int id = getId(name+index);
            // inserisco domanda
            CorrectPart4_domande(id, risposte[i]);
        }

        TextView tw = findViewById(R.id.parte4_punti);
        String ris = "Fourth part points: "+points_part4+"/8";
        tw.setText(ris);

        tw = findViewById(R.id.tot_punti);
        int tot = points_part1 + points_part2 + points_part3 + points_part4;
        String tot_string = "Overall points: "+tot+"/42";
        tw.setText(tot_string);
    }

    public void CorrectPart4_domande(int id, String answer){
        EditText editText = findViewById(id);
        String attempt = editText.getText().toString();

        if (attempt.trim().toUpperCase().compareTo(answer.trim().toUpperCase()) == 0){
            points_part4++;
            editText.setTextColor(Color.GREEN);
        }else{
            editText.setText(answer);
            editText.setTextColor(Color.RED);
        }

    }

    // Asyntask per parsing file json
    private class ParseJsonAT extends AsyncTask<String, Void, ParseJson> {

        @Override
        protected ParseJson doInBackground(String... s) {
            String nome_file = s[0];

            // faccio parsing del file json
            Log.d("PJDM","nome file: "+nome_file);
            int id = UseOfEnglishActivity.this.getResources().getIdentifier(nome_file,"raw",UseOfEnglishActivity.this.getPackageName());
            InputStream is = UseOfEnglishActivity.this.getResources().openRawResource(id);
            json_parse = new ParseJson(is);

            // "titolo" -> da cambiare!!
            String titolo = "Use of English " + json_parse.getLevel()+" - "+json_parse.getSeries();
            TextView tw = findViewById(R.id.textView3);
            tw.setText(titolo);

            // chiudo file
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return json_parse;
        }

        @Override
        protected void onPostExecute(ParseJson json) {
            // INSERISCO I DATI - PARTE1 nota: domandaParte1 = [domandax][domanda,s1,s2,s3]
            RiempiDomandeParte1(json);

            // INSERISCO I DATI - PARTE2
            RiempiDomandeParte2(json);

            // INSERISCO I DATI - PARTE3 nota: domandaParte2 = [domandax][domandax-1,risposta-x,domandax-2]
            RiempiDomandeParte3(json);

            RiempiDomandeParte4(json);

            // inserisco titoli
            insertText(R.id.title_first_part, json_parse.getTitle_first_part());
            insertText(R.id.title_second_part, json_parse.getTitle_second_part());
            insertText(R.id.title_third_part, json_parse.getTitle_third_part());
            insertText(R.id.title_fourth_part, json_parse.getTitle_fourth_part());
        }
    }


}
