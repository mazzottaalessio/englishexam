package com.example.ale.englishexam;

public class Definition {
    private String definition;
    private String word;
    private int num_element;

    public Definition(){
    }

    public int getNum_element() {
        return num_element;
    }

    public void setNum_element(int num_element) {
        this.num_element = num_element;
    }

    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }
}

