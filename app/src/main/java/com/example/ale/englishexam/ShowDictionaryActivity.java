package com.example.ale.englishexam;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;


public class ShowDictionaryActivity extends AppCompatActivity {

    MyDictionaryDBHelper DBHelper;
    SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_dictionary);

        DBHelper = new MyDictionaryDBHelper(this);
        db = DBHelper.getWritableDatabase();

        String orderBY = MyDictionaryDB.MyDictionaryEntry.COLUMN_NAME_WORD + " ASC";
        Cursor cursor = db.query(MyDictionaryDB.MyDictionaryEntry.TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                orderBY);
        int number_definition = cursor.getCount();
        Definition[] definition = new Definition[number_definition];
        for (int i = 0; i < number_definition; i++){
            definition[i] = new Definition();
        }

        // faccio parsing di cursor mettendo il contenuto in array di oggetti definition

        for (int i = 0; i < number_definition; i++){
            cursor.moveToPosition(i);
            Definition object_def = definition[i];
            String word = cursor.getString(2);
            String def = cursor.getString(1);

            Log.d("PJDM","def: "+def);
            Log.d("PJDM","word: "+word);
            object_def.setDefinition(def);
            object_def.setWord(word.toUpperCase());
        }
        cursor.close();
        db.close();

        ListView listView = findViewById(R.id.db_item);
        MyArrayAdapterDBShow myArrayAdapterDBShow = new MyArrayAdapterDBShow(this,R.layout.my_item_db, definition);
        listView.setAdapter(myArrayAdapterDBShow);
    }
}
