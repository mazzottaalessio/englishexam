package com.example.ale.englishexam;

import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class Listening extends AppCompatActivity {

    private Timer timer;

    private String traccia_audio;
    private MediaPlayer player;
    private ParseJson json_parse;

    private int points_part1 = 0;
    private int points_part2 = 0;
    private int points_part3 = 0;
    private int points_part4 = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listening);

        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey("nome_file")){
            String nome_file = extras.getString("nome_file");

            ParseJsonAT parseJsonAT = new ParseJsonAT();
            parseJsonAT.execute(nome_file);

        }

    }

    // se si preme indietro fermo la registrazione
    @Override
    public void onBackPressed() {
        if (timer.ismTimerRunning()){
            timer.pauseTimer();
            pausePlayer();
        }
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        if (timer.ismTimerRunning()){
            timer.pauseTimer();
            pausePlayer();
        }
        super.onPause();
    }

    // TIMER E AUDIO

    // PARTE TIMER
    public void startPause(View view) {
        // se il tempo scorre
        if (timer.ismTimerRunning()) {
            timer.pauseTimer();
            pausePlayer();
        } else {
            timer.startTimer();
            playPlayer(traccia_audio);
        }

    }

    // PARTE AUDIO

    private void playPlayer(String traccia_audio) {
        if (player == null) {

            int id = Listening.this.getResources().getIdentifier(traccia_audio,"raw",Listening.this.getPackageName());
            player = MediaPlayer.create(this, id);
            // faccio rilasciare il media player al termine del file audio
            player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    stopPlayer();
                }
            });
        }
        player.start();
    }

    private void stopPlayer() {
        if (player != null) {
            player.release();
            player = null;
            // messaggio a schermo
            Toast.makeText(this, "media player released", Toast.LENGTH_SHORT).show();
        }
    }

    private void pausePlayer() {
        if (player != null) {
            player.pause();
        }
    }

    // FINE PARTE AUDIO

    private void insertText(int id, String question){
        TextView textView = findViewById(id);
        textView.setText(question);
    }

    private void insertRadioBotton(int id, String question){
        RadioButton radioButton = findViewById(id);
        radioButton.setText(question);
    }

    private int getId(String name){
        int id = 0;
        try {
            id = R.id.class.getField(name).getInt(null);
        } catch (IllegalAccessException | NoSuchFieldException e) {
            e.printStackTrace();
        }
        return id;
    }

    // RIEMPI PARTE 1
    private void RiempiDomandeParte1() {
        json_parse.parseParte1();
        String[][] domandeparte1 = json_parse.getDomandeParte1();

        int domande = 8;
        int possibili_risposte = 3;
        String name = "parte1_domanda";
        for (int i = 0; i < domande; i++){
            int index = i + 1;
            int id = getId(name+index);
            // inserisco domanda
            insertText(id,domandeparte1[i][0]);
            for (int j = 0; j < possibili_risposte; j++){
                // inserisco possibili risposte
                String name_rb = "rbparte1_d";
                int index2 = j+1;
                int id_rb = getId(name_rb+index+"_"+index2);
                insertRadioBotton(id_rb,domandeparte1[i][j+1]);

            }
        }
    }

    // RIEMPI PARTE 2
    private void RiempiDomandeParte2() {
        json_parse.parseParte2();
        String[][] domandeparte2 = json_parse.getDomandeParte2();

        int domande = 10;
        String name = "parte2_domanda";
        for (int i = 0; i < domande; i++){
            int index = i + 1;
            int id = getId(name+index+"_1");
            // inserisco domanda
            insertText(id,domandeparte2[i][0]);

        }

    }

    // RIEMPI PARTE 3
    private void RiempiDomandeParte3() {
        json_parse.parseParte3();
        String[][] domandeparte3 = json_parse.getDomandeParte3();

        int domande = 7;
        String name = "parte3_domanda";

        for (int i = 0; i < domande; i++){
            int index = i + 1;
            int id = getId(name+index);
            // inserisco domanda
            insertText(id,domandeparte3[i][0]);

        }

        // spinner
        String[] choices = {"A","B","C","D","E","F","G"};
        riempiSpinner(R.id.spinner1, choices);
        riempiSpinner(R.id.spinner2, choices);
        riempiSpinner(R.id.spinner3, choices);
        riempiSpinner(R.id.spinner4, choices);
        riempiSpinner(R.id.spinner5, choices);

    }

    private void riempiSpinner(int id, String[] choices){
        Spinner spinner = findViewById(id);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this,R.layout.spinner_element,choices);
        spinner.setAdapter(arrayAdapter);
    }


    // RIEMPI PARTE 4
    private void RiempiDomandeParte4() {
        json_parse.parseParte4();
        String[][] domandeparte4 = json_parse.getDomandeParte4();

        int domande = 7;
        int possibili_risposte = 3;
        String name = "twparte4_d";
        for (int i = 0; i < domande; i++){
            int index = i + 1;
            int id = getId(name+index);
            // inserisco domanda
            insertText(id,domandeparte4[i][0]);
            for (int j = 0; j < possibili_risposte; j++){
                // inserisco possibili risposte
                String name_rb = "twparte4_d";
                int index2 = j+1;
                int id_rb = getId(name_rb+index+"_b"+index2);
                insertRadioBotton(id_rb,domandeparte4[i][j+1]);
            }
        }

    }

    /* ---------------------------- CORREZZIONE  ------------------------------------------*/

    public void correct_test(View view) {
        CorrectPart1();
        correctPart2();
        correctPart3();
        correctPart4();

        // rendo non cliccabile il bottone correggi
        Button btt_end = findViewById(R.id.bttend);
        btt_end.setClickable(false);
        //btt_end.setVisibility(View.INVISIBLE);
    }

    // corregge la parte 1 e inserisce
    public void CorrectPart1(){
        final String[] risposteparte1 = json_parse.getRisposteParte1();

        int domande = 8;
        String name = "rgparte1_";
        for (int i = 0; i < domande; i++){
            int index = i + 1;
            int id = getId(name+index);
            // correggo domanda i
            CorrectPart1_domande(id, risposteparte1[i]);

        }

        TextView tw = findViewById(R.id.parte1_punti);
        String ris = "First part points: "+points_part1+"/8";
        tw.setText(ris);

    }

    public void CorrectPart1_domande(int rg_id, String answer){
        RadioGroup rg = findViewById(rg_id);

        int id = rg.getCheckedRadioButtonId();
        if (id != -1){
            RadioButton rb = findViewById(id);
            String attempt = rb.getText().toString();

            if (attempt.compareTo(answer) == 0){
                points_part1++;
                rb.setBackgroundColor(Color.GREEN);
            }else{
                rb.setBackgroundColor(Color.RED);
                correctPart1_radioButton(rg, answer);

            }

        }else{
            correctPart1_radioButton(rg, answer);
        }

    }

    public void correctPart1_radioButton(RadioGroup rg, String answer){
        RadioButton rb;
        String attempt;
        int count = rg.getChildCount();
        ArrayList<RadioButton> listOfRadioButtons = new ArrayList<>();
        for (int i=0;i<count;i++) {
            View o = rg.getChildAt(i);

            if (o instanceof RadioButton) {
                listOfRadioButtons.add((RadioButton)o);
            }
        }

        for (int i=0; i<listOfRadioButtons.size(); i++ ){
            rb = listOfRadioButtons.get(i);
            attempt = rb.getText().toString();
            if (attempt.compareTo(answer) == 0) {
                rb.setBackgroundColor(Color.GREEN);
            }
        }

    }

    public void correctPart2(){
        final String[] risposteparte2 = json_parse.getRisposteParte2();

        int domande = 10;
        String name = "parte2_risposta";
        for (int i = 0; i < domande; i++){
            int index = i + 1;
            int id = getId(name+index);
            // correggo domanda i
            CorrectPart2_domande(id, risposteparte2[i]);

        }

        TextView tw = findViewById(R.id.parte2_punti);
        String ris = "Second part points: "+points_part2+"/10";
        tw.setText(ris);

    }

    public void CorrectPart2_domande(int tw_id, String answer){
        TextView tw = findViewById(tw_id);
        String attempt = tw.getText().toString().toLowerCase().trim();

        if (attempt.compareTo(answer.toLowerCase()) == 0){
            points_part2++;
            tw.setTextColor(Color.GREEN);
        }else{
            tw.setText(answer);
            tw.setTextColor(Color.RED);
        }

    }

    public void correctPart3(){
        final String[] risposteparte3 = json_parse.getRisposteParte3();

        int domande = 5;
        String name = "spinner";
        for (int i = 0; i < domande; i++){
            int index = i + 1;
            int id = getId(name+index);
            // correggo domanda i
            CorrectPart3_domande(id, risposteparte3[i]);

        }

        TextView tw = findViewById(R.id.parte3_punti);
        String ris = "Second part points: "+points_part3+"/5";
        tw.setText(ris);


    }

    public void CorrectPart3_domande(int tw_id, String answer){
        Spinner s = findViewById(tw_id);
        String attempt = s.getSelectedItem().toString().toLowerCase().trim();

        if (attempt.compareTo(answer.toLowerCase().trim()) == 0){
            points_part3++;
            s.setBackgroundColor(Color.GREEN);
        }else{
            int position = answer.charAt(0) - 65; // nota A = 65
            s.setSelection(position);
            s.setBackgroundColor(Color.RED);
        }
    }

    public void correctPart4(){
        final String[] risposteparte4 = json_parse.getRisposteParte4();

        int domande = 7;
        String name = "rgparte4_d";
        for (int i = 0; i < domande; i++){
            int index = i + 1;
            int id = getId(name+index);
            // correggo domanda i
            CorrectPart4_domande(id, risposteparte4[i]);

        }

        TextView tw = findViewById(R.id.parte4_punti);
        String ris = "Second part points: "+points_part4+"/7";
        tw.setText(ris);

        TextView tw1 = findViewById(R.id.tot_punti);
        int tot =  points_part4+points_part3+points_part2+points_part1;
        String tot_string = "Overall points: "+tot+"/30";
        tw1.setText(tot_string);
    }

    public void CorrectPart4_domande(int rg_id, String answer){
        RadioGroup rg = findViewById(rg_id);

        int id = rg.getCheckedRadioButtonId();
        // se nulla è stato selezionato metto in rosso l'intero RadioGroup.
        if (id != -1){
            RadioButton rb = findViewById(id);
            String attempt = rb.getText().toString();

            if (attempt.compareTo(answer) == 0){
                points_part4++;
                rb.setBackgroundColor(Color.GREEN);
            }else{
                rb.setBackgroundColor(Color.RED);
                correctPart1_radioButton(rg, answer);
            }

        }else{
            correctPart1_radioButton(rg, answer);
        }
    }



    // Asyntask per parsing file json
    private class ParseJsonAT extends AsyncTask<String, Void, ParseJson> {

        @Override
        protected ParseJson doInBackground(String... s) {
            String nome_file = s[0];

            // faccio parsing del file json
            int id = Listening.this.getResources().getIdentifier(nome_file,"raw",Listening.this.getPackageName());
            InputStream is = Listening.this.getResources().openRawResource(id);

            json_parse = new ParseJson(is);

            // chiudo file
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return json_parse;
        }

        @Override
        protected void onPostExecute(ParseJson json) {

            // "titolo" -> da cambiare!!
            String titolo = "Listening " + json_parse.getLevel()+" - "+json_parse.getSeries();
            TextView tw = findViewById(R.id.textView3);
            tw.setText(titolo);

            // traccia audio
            traccia_audio = json_parse.getAudio();

            // prendo durata traccia:
            int id_audio = Listening.this.getResources().getIdentifier(traccia_audio,"raw",Listening.this.getPackageName());
            player = MediaPlayer.create(getApplicationContext(), id_audio);
            int timeDuration = player.getDuration();
            player = null;

            // Timer
            TextView tw_timer = findViewById(R.id.timer);
            Button btt_startPause = findViewById(R.id.startPause);
            timer = new Timer(timeDuration, tw_timer, btt_startPause,getApplicationContext());

            // INSERISCO I DATI - PARTE1 nota: domandaParte1 = [domandax][domanda,s1,s2,s3]
            RiempiDomandeParte1();

            // INSERISCO I DATI - PARTE2 nota: domandaParte2 = [domandax][domandax-1,risposta-x,domandax-2]
            RiempiDomandeParte2();

            // INSERISCO I DATI - PARTE3 nota: domandaParte3 = [domandax][domandax]
            RiempiDomandeParte3();

            // INSERISCO I DATI - PARTE4 nota: domandaParte4 = [domandax][domanda,s1,s2,s3]
            RiempiDomandeParte4();

            // inserisco titoli
            insertText(R.id.title_first_part, json_parse.getTitle_first_part());
            insertText(R.id.title_second_part, json_parse.getTitle_second_part());
            insertText(R.id.title_third_part, json_parse.getTitle_third_part());
            insertText(R.id.title_fourth_part, json_parse.getTitle_fourth_part());
        }
    }
}
