package com.example.ale.englishexam;

import android.app.Activity;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class ThreadReader extends Thread {

    private Definition def;
    private int tot;
    private ArrayList listDef;
    private Activity activity;
    private TextView tw_definition;
    private Timer timer;
    private int duration;
    private TextView tw_timer;

    private HttpURLConnection urlConnection;
    private final String IP = "http://192.168.178.46:8080/ProvaServlet/English?id=";


    public ThreadReader(Definition def, int tot, ArrayList list, Activity activity, TextView textView, Timer timer, int duration, TextView tw){
        this.def = def;
        this.tot = tot;
        this.listDef = list;
        this.activity = activity;
        this.tw_definition = textView;
        this.timer = timer;
        this.duration = duration;
        this.tw_timer = tw;
    }



    public void run(){
        try {
            int attempt = tot - 1;
            int id = (int) Math.round(Math.random()*(tot-1) + 1);

            int index = listDef.indexOf(id);
            while (index != -1){
                id = (int) Math.round(Math.random()*(tot-1) + 1);
                index = listDef.indexOf(id);
                attempt--;
                // se #tentativi >= #elementi => smetto iterare e prendo def già capitata
                if (attempt <= 0){
                    break;
                }
            }
            listDef.add(id);
            URL url = new URL(IP+id);

            Log.d("PJDM",url+"");
            urlConnection = (HttpURLConnection) url.openConnection();
            Log.d("PJDM","ho aperto la connessione");
            InputStream is = urlConnection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            Log.d("PJDM","dopo bufferedReader");
            String json = "";
            String line = reader.readLine();
            Log.d("PJDM",line);
            while(line != null){
                json+=line;
                line = reader.readLine();
            }
            JSONObject jo = new JSONObject(json);
            final String definition = jo.getString("definition");
            String word = jo.getString("word");


            def.setDefinition(definition);
            def.setWord(word);

            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    tw_definition.setText(definition);
                }
            });

            is.close();
            reader.close();
            //urlConnection.disconnect();

        } catch (IOException e) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(activity.getBaseContext(),"Connection problem, please check your connection",Toast.LENGTH_SHORT).show();
                    def.setWord(null);

                    // fermo il timer
                    if (timer.ismTimerRunning()){
                        timer.pauseTimer();
                        timer.setEnd(true);
                        timer.setmTimeLeftInMillis(duration);
                        tw_timer.setText(R.string.timer);
                    }

                }
            });
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }finally {
            urlConnection.disconnect();

        }
    }
}
