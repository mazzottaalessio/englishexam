package com.example.ale.englishexam;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void Listening(View view) {
        // PROVA
        Intent intent = new Intent(this, exam_list.class);
        intent.putExtra("type","listening");
        startActivity(intent);
    }

    public void Reading(View view){
        Intent intent = new Intent(this, exam_list.class);
        intent.putExtra("type","reading");
        startActivity(intent);
    }

    public void useOfEnglish(View view) {
        Intent intent = new Intent(this, exam_list.class);
        intent.putExtra("type","useofenglish");
        startActivity(intent);
    }

    public void Speaking(View view){
        Intent intent = new Intent(this, exam_list.class);
        intent.putExtra("type","speaking");
        startActivity(intent);
    }

    public void Writing(View view){
        Intent intent = new Intent(this, exam_list.class);
        intent.putExtra("type","writing");
        startActivity(intent);
    }

    public void Guess(View view) {
        Intent intent = new Intent(this, Guess.class);
        startActivity(intent);
    }

    public void MyDictionaray(View view) {
        Intent intent = new Intent(MainActivity.this,MyDictionary.class);
        startActivity(intent);
    }
}
