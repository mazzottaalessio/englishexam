package com.example.ale.englishexam;

import android.provider.BaseColumns;

public class MyDictionaryDB {

    private MyDictionaryDB(){

    }

    public static class MyDictionaryEntry implements BaseColumns {
        public static final String TABLE_NAME = "myDictionary";
        public static final String COLUMN_NAME_DEF = "definition";
        public static final String COLUMN_NAME_WORD = "word";

    }
}
