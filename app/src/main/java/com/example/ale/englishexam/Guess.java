package com.example.ale.englishexam;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;

public class Guess extends AppCompatActivity {

    private Timer timer;

    private Definition def;
    private int number_definition = 0;

    private int correct_answer = 0;
    private MediaPlayer mediaPlayer = null;

    private int duration = 1000*60*2; // 2 minuti
    private ArrayList listDef; // per controllare che non ci siano ripetizioni

    private ThreadGetNumber tgn;
    private TextView tw_timer;
    int best_score;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guess);

        // inserisco miglior punteggio con thread
        TextView tv_score = findViewById(R.id.best_score);
        //tv_definition = findViewById(R.id.definition);

        SharedPreferences sharedPreferencesInt = PreferenceManager.getDefaultSharedPreferences(this);
        // il secondo valore è il valore di default
        best_score = sharedPreferencesInt.getInt("best_score",0);
        tv_score.setText(R.string.best_score);
        tv_score.append(" " + best_score);

        // Timer
        int timeDuration = duration;
        tw_timer = findViewById(R.id.timer);
        Button btt_startPause = findViewById(R.id.startPause);
        timer = new Timer(timeDuration, tw_timer, btt_startPause,this);

        // inizializza oggetto definition
        def = new Definition();

        // creo listArray vuoto
        listDef = new ArrayList();

        tgn = new ThreadGetNumber(def,this, timer, duration,tw_timer);
        tgn.start();
    }

    // se si preme indietro fermo timer
    @Override
    public void onBackPressed() {
        if (timer.ismTimerRunning()){
            timer.pauseTimer();
        }
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        if (timer.ismTimerRunning()){
            timer.pauseTimer();
        }
        super.onPause();
    }

    // PARTE TIMER and START ESERCIZIO
    public void startPause(View view) {

        if (timer.isEnd()){
            newGame();
            return;
        }

        if (timer.ismTimerRunning()) {
            timer.pauseTimer();
        } else {
            timer.startTimer();
            Change_definition();
        }

    }

    public void check_word(View view) {

        // se il tempo non sta scorrendo non posso fare i check delle parole!
        if (!timer.ismTimerRunning()){
            return;
        }

        //definition = def.getDefinition();
        String word = def.getWord();

        EditText et = findViewById(R.id.answer);
        String attempt = et.getText().toString();

        // non può effettuare il check
        if(word == null){
            return;
        }
        if (attempt.toUpperCase().trim().compareTo(word.toUpperCase().trim()) == 0){
            start_sound(R.raw.correct);
            correct_answer++;
            update_currentScore(correct_answer);
            Change_definition();
            et.setText("");

            TextView textView = findViewById(R.id.last_answer);
            textView.setText(attempt);
            textView.setTextColor(Color.GREEN);

        }else{
            start_sound(R.raw.wrong);
            TextView textView = findViewById(R.id.last_answer);
            textView.setText(attempt);
            textView.setTextColor(Color.RED);

        }
    }

    public void Change_definition(){
        number_definition = def.getNum_element();
        if (number_definition == -1){
            Toast.makeText(this,"please check your connection",Toast.LENGTH_SHORT).show();
            // se per problemi di connessione ho #definition = -1 => riprendi numero connessioni
            tgn = new ThreadGetNumber(def,this, timer, duration, tw_timer);
            tgn.start();

            return;
        }
        TextView tv = findViewById(R.id.definition);
        ThreadReader tr = new ThreadReader(def, number_definition, listDef, this, tv,timer,duration,tw_timer);
        tr.start();


    }

    public void newGame(){
        // rinizializzo listArray vuoto
        number_definition = def.getNum_element();
        listDef = new ArrayList();

        timer.pauseTimer();
        timer.ismTimerRunning(false);
        long time_duration = duration;
        timer.setmTimeLeftInMillis(time_duration);
        correct_answer = 0;
        update_currentScore(correct_answer);

        EditText et = findViewById(R.id.answer);
        et.setText("");

        if (number_definition <= -1){
            tgn = new ThreadGetNumber(def,this, timer, duration,tw_timer);
            tgn.start();
            return;
        }

        // metto anche parte start
        timer.startTimer();
        Change_definition();
    }

    public void update_currentScore(int correct_answer){
        TextView textView = findViewById(R.id.current_score);
        textView.setText(R.string.current_score);
        textView.append(" " + correct_answer);

        if (correct_answer > best_score){
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putInt("best_score", correct_answer);
            editor.apply();

            update_score(R.id.best_score,correct_answer);
        }
    }

    public void update_score(int id_tv, int best_score){
        TextView textView = findViewById(id_tv);
        textView.setText(R.string.best_score);
        textView.append(" " + best_score);
    }

    public void start_sound(int id_sound){
        if (mediaPlayer != null){
            mediaPlayer.stop();
            mediaPlayer.release();
        }
        mediaPlayer = MediaPlayer.create(this, id_sound);
        mediaPlayer.start();
    }

    public void show_answer(View view) {

        TextView textView = findViewById(R.id.last_answer);
        textView.setText(def.getWord());
        textView.setTextColor(Color.RED);
        if (timer.ismTimerRunning()){
            Change_definition();
        }

    }
}
