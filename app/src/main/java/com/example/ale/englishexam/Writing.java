package com.example.ale.englishexam;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;

public class Writing extends AppCompatActivity {

    //private TextView tw_timer;
    //private Button btt_startPause;
    //private long timeDuration = 80*60*1000; // 1 ora e 20 minuti

    private Timer timer;

    private ParseJson json_parse;
    //private ParseJsonAT parseJsonAT;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_writing);

        // Timer
        TextView tw_timer = findViewById(R.id.timer);
        Button btt_startPause = findViewById(R.id.startPause);
        long timeDuration = 80*60*1000; // 1 ora e 20 minuti
        timer = new Timer(timeDuration, tw_timer, btt_startPause,this);

        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey("nome_file")){
            String nome_file = extras.getString("nome_file");

            ParseJsonAT parseJsonAT = new ParseJsonAT();
            parseJsonAT.execute(nome_file);
        }

    }

    @Override
    protected void onPause() {
        if (timer.ismTimerRunning()){
            timer.pauseTimer();
        }
        super.onPause();
    }


    // se si preme indietro fermo timer
    @Override
    public void onBackPressed() {
        if (timer.ismTimerRunning()){
            timer.pauseTimer();
        }
        super.onBackPressed();
    }

    // PARTE TIMER
    public void startPause(View view) {
        // se il tempo scorre
        if (timer.ismTimerRunning()) {
            timer.pauseTimer();
        } else {
            timer.startTimer();
        }

    }

    // INSERT PARTE 1

    private void RiempiDomandeParte1(ParseJson json_parse) {
        json_parse.parseParte1_writing();
        String[][] domandeparte1 = json_parse.getDomandeParte1();

        insertText(R.id.twparte1_question1,domandeparte1[0][0]);
        insertText(R.id.twparte1_question2,domandeparte1[0][1]);
        insertText(R.id.twparte1_question3,domandeparte1[0][2]);


    }

    private void insertText(int id, String question){
        TextView textView = findViewById(id);
        textView.setText(question);
    }


    // INSERT PARTE 2

    private void RiempiDomandeParte2(ParseJson json_parse) {
        json_parse.parseParte2_writing();
        String[][] domandeparte2 = json_parse.getDomandeParte2();

        insertText(R.id.twparte2_d1_question1,domandeparte2[0][0]);
        insertText(R.id.twparte2_d1_question2,domandeparte2[0][1]);
        insertText(R.id.twparte2_d1_question3,domandeparte2[0][2]);

        insertText(R.id.twparte2_d2_question1,domandeparte2[1][0]);
        insertText(R.id.twparte2_d2_question2,domandeparte2[1][1]);
        insertText(R.id.twparte2_d2_question3,domandeparte2[1][2]);

        insertText(R.id.twparte2_d3_question1,domandeparte2[2][0]);
        insertText(R.id.twparte2_d3_question2,domandeparte2[2][1]);
        insertText(R.id.twparte2_d3_question3,domandeparte2[2][2]);

        insertText(R.id.twparte2_d4_question1,domandeparte2[3][0]);
        insertText(R.id.twparte2_d4_question2,domandeparte2[3][1]);
        insertText(R.id.twparte2_d4_question3,domandeparte2[3][2]);
    }

    // Asyntask per parsing file json
    private class ParseJsonAT extends AsyncTask<String, Void, ParseJson> {

        @Override
        protected ParseJson doInBackground(String... s) {
            String nome_file = s[0];

            // faccio parsing del file json
            Log.d("PJDM","nome file: "+nome_file);
            int id = Writing.this.getResources().getIdentifier(nome_file,"raw",Writing.this.getPackageName());
            InputStream is = Writing.this.getResources().openRawResource(id);
            //Parse(is);
            json_parse = new ParseJson(is);

            // "titolo"
            String titolo = "Writing " + json_parse.getLevel()+" - "+json_parse.getSeries();
            TextView tw = findViewById(R.id.textView3);
            tw.setText(titolo);

            // chiudo file
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return json_parse;
        }

        @Override
        protected void onPostExecute(ParseJson json) {

            String titolo = "Writing " + json_parse.getLevel()+" - "+json_parse.getSeries();
            TextView tw = findViewById(R.id.textView3);
            tw.setText(titolo);

            // INSERISCO I DATI - PARTE1 nota: domandaParte1 = [domandax][domanda,s1,s2,s3]
            RiempiDomandeParte1(json);

            // INSERISCO I DATI - PARTE2 nota: domandaParte3 = [domandax][domandax]
            RiempiDomandeParte2(json);
        }
    }
}
