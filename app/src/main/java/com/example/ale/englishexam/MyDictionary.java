package com.example.ale.englishexam;

import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MyDictionary extends AppCompatActivity {

    //private MyDictionaryDBHelper DBHelper;
    private SQLiteDatabase db;
    private int duration = 1000*60; // 1 minuto
    private Timer timer;

    private Definition def;
    private int number_definition;

    private ArrayList listDef;

    private int correct_answer = 0;
    private MediaPlayer mediaPlayer = null;

    // Thread per leggere definizione da DB
    private ThreadGetDefinition threadGetDefinition;
    private TextView tv_definition;

    // parte scelta operazione
    private LinearLayout insert_layout;
    private LinearLayout delete_layout;
    private LinearLayout replace_layout;
    private LinearLayout check_layout;

    private int best_score;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_dictionary);

    }

    @Override
    protected void onStart() {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        duration = (Integer.parseInt(sharedPreferences.getString("duration", "1"))*1000*60);
        Log.d("PJDM","duration in onStart: "+duration);

        insert_layout = findViewById(R.id.insert_layout);
        delete_layout = findViewById(R.id.delete_layout);
        replace_layout = findViewById(R.id.replace_layout);
        check_layout = findViewById(R.id.check_layout);

        RadioGroup rg_operation = findViewById(R.id.rg_operation);
        rg_operation.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.rb_insert:
                        insert_layout.setVisibility(View.VISIBLE);
                        delete_layout.setVisibility(View.GONE);
                        replace_layout.setVisibility(View.GONE);
                        check_layout.setVisibility(View.GONE);
                        // se timer attivo...
                        if (timer.ismTimerRunning()){
                            timer.pauseTimer();
                        }
                        break;
                    case R.id.rb_delete:
                        insert_layout.setVisibility(View.GONE);
                        delete_layout.setVisibility(View.VISIBLE);
                        replace_layout.setVisibility(View.GONE);
                        check_layout.setVisibility(View.GONE);
                        // se timer attivo...
                        if (timer.ismTimerRunning()){
                            timer.pauseTimer();
                        }
                        break;
                    case R.id.rb_replace:
                        insert_layout.setVisibility(View.GONE);
                        delete_layout.setVisibility(View.GONE);
                        replace_layout.setVisibility(View.VISIBLE);
                        check_layout.setVisibility(View.GONE);
                        // se timer attivo...
                        if (timer.ismTimerRunning()){
                            timer.pauseTimer();
                        }
                        break;
                    case R.id.rb_check:
                        insert_layout.setVisibility(View.GONE);
                        delete_layout.setVisibility(View.GONE);
                        replace_layout.setVisibility(View.GONE);
                        check_layout.setVisibility(View.VISIBLE);
                        // se timer attivo...
                        if (timer.ismTimerRunning()){
                            timer.pauseTimer();
                        }
                        break;

                }
            }
        });

        // apro DB
        MyDictionaryDBHelper DBHelper = new MyDictionaryDBHelper(this);
        db = DBHelper.getWritableDatabase();

        // ottengo numero righe (numero definition - word)
        Cursor cursor_count = db.query(MyDictionaryDB.MyDictionaryEntry.TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                null);
        number_definition = cursor_count.getCount();
        cursor_count.close();

        if (number_definition == 0){
            Toast.makeText(this,"There are no word in your dictionary, insert some word to play", Toast.LENGTH_SHORT).show();
        }

        // inserisco miglior punteggio
        tv_definition = findViewById(R.id.definition);

        SharedPreferences sharedPreferencesInt = PreferenceManager.getDefaultSharedPreferences(this);
        // il secondo valore è il valore di default
        best_score = sharedPreferencesInt.getInt("best_score",0);
        update_score(R.id.best_score,best_score);

        // creo listArray vuoto
        listDef = new ArrayList();

        // inizializza oggetto definition
        def = new Definition();

        // Timer
        int timeDuration = duration;
        TextView tw_timer = findViewById(R.id.timer);
        Button btt_startPause = findViewById(R.id.startPause);
        timer = new Timer(timeDuration, tw_timer, btt_startPause,this);

        super.onStart();
    }

    // se si preme indietro fermo timer
    @Override
    public void onBackPressed() {
        if (timer.ismTimerRunning()){
            timer.pauseTimer();
        }
        db.close();
        super.onBackPressed();
    }

    @Override
    protected void onStop() {
        if (timer.ismTimerRunning()){
            timer.pauseTimer();
        }
        db.close();
        super.onStop();
    }

    // al click di insert
    public void insert(View view) {
        EditText et_word_ins = findViewById(R.id.word_ins);
        String word_ins = et_word_ins.getText().toString();

        EditText et_def_ins = findViewById(R.id.definition_ins);
        String def_ins = et_def_ins.getText().toString();

        if ((word_ins.compareTo("") == 0) || (def_ins.compareTo("") == 0 )){
            Toast.makeText(this,"please, set both definition and word",Toast.LENGTH_SHORT).show();
            return;
        }

        // inserisco valore
        ContentValues values = new ContentValues();
        values.put(MyDictionaryDB.MyDictionaryEntry.COLUMN_NAME_DEF, def_ins);
        values.put(MyDictionaryDB.MyDictionaryEntry.COLUMN_NAME_WORD, word_ins.toLowerCase().trim());

        db.insert(MyDictionaryDB.MyDictionaryEntry.TABLE_NAME, null,values);

        Toast.makeText(this,"This new word is now on your Dictionary",Toast.LENGTH_SHORT).show();
        et_def_ins.setText("");
        et_word_ins.setText("");

        // incremento numero di definizioni
        number_definition++;

    }


    public void startPause(View view) {

        if (number_definition <= 0){
            Toast.makeText(this,"There are no definition, please insert at least one", Toast.LENGTH_SHORT).show();
            return;
        }

        if (timer.isEnd()){
            newGame();
            return;
        }

        if (timer.ismTimerRunning()) {
            timer.pauseTimer();
        } else {
            timer.startTimer();
            //Change_definition();

            threadGetDefinition = new ThreadGetDefinition(number_definition, def, listDef,
                    db, this, tv_definition);
            threadGetDefinition.start();
        }
    }

    public void check_word(View view) {
        // se non ho inizializzato il timer non posso fare i check delle parole!
        if (timer.isEnd()){
            return;
        }

        // se il tempo non sta scorrendo non posso fare i check delle parole!
        if (!timer.ismTimerRunning()){
            return;
        }

        EditText et = findViewById(R.id.answer);
        String attempt = et.getText().toString();

        Log.d("PJDM","attempt in main: "+attempt);
        Log.d("PJDM","word in main: "+def.getWord());
        if (attempt.toUpperCase().trim().compareTo(def.getWord().toUpperCase().trim()) == 0){
            //et.setTextColor(Color.GREEN);
            start_sound(R.raw.correct);
            correct_answer++;
            update_currentScore(correct_answer);
            //Change_definition();

            threadGetDefinition = new ThreadGetDefinition(number_definition, def, listDef,
                    db, this, tv_definition);
            threadGetDefinition.start();

            et.setText("");

            TextView textView = findViewById(R.id.last_answer);
            textView.setText(attempt);
            textView.setTextColor(Color.GREEN);
        }else{
            start_sound(R.raw.wrong);
            TextView textView = findViewById(R.id.last_answer);
            textView.setText(attempt);
            textView.setTextColor(Color.RED);
        }
    }

    public void newGame(){
        // rinizializzo listArray vuoto
        listDef = new ArrayList();

        timer.pauseTimer();
        timer.ismTimerRunning(false);

        long time_duration = duration;

        timer.setmTimeLeftInMillis(time_duration);
        correct_answer = 0;
        update_currentScore(correct_answer);

        EditText et = findViewById(R.id.answer);
        et.setText("");

        // metto anche parte start
        timer.startTimer();

        threadGetDefinition = new ThreadGetDefinition(number_definition, def, listDef,
                db, this, tv_definition);
        threadGetDefinition.start();


    }

    public void update_currentScore(int correct_answer){
        TextView textView = findViewById(R.id.current_score);
        textView.setText(R.string.current_score);
        textView.append(" "+correct_answer);

        if (correct_answer > best_score){

            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putInt("best_score", correct_answer);
            editor.apply();

            //write_best_score(correct_answer);
            update_score(R.id.best_score,correct_answer);

        }
    }

    public void update_score(int id_tv, int best_score){
        TextView textView = findViewById(id_tv);
        textView.setText(R.string.best_score);
        textView.append(" "+best_score);
    }

    public void start_sound(int id_sound){
        if (mediaPlayer != null){
            mediaPlayer.stop();
            mediaPlayer.release();
        }
        mediaPlayer = MediaPlayer.create(this, id_sound);
        mediaPlayer.start();
    }

    public void show_answer(View view) {

        if (number_definition == 0){
            Toast.makeText(this,"There are no word in your dictionary, insert some word to play", Toast.LENGTH_SHORT).show();
            return;
        }
        TextView textView = findViewById(R.id.last_answer);
        textView.setText(def.getWord());
        textView.setTextColor(Color.RED);
        if (!timer.isEnd()){
            //Change_definition();

            threadGetDefinition = new ThreadGetDefinition(number_definition, def, listDef,
                    db, this, tv_definition);
            threadGetDefinition.start();
        }


    }

    public void replace(View view) {
        EditText et_old_word = findViewById(R.id.old_word_replace);
        String old_word = et_old_word.getText().toString().toLowerCase().trim();

        EditText et_new_word = findViewById(R.id.new_word_replace);
        String new_word = et_new_word.getText().toString();

        EditText et_new_definition = findViewById(R.id.definition_replace);
        String new_definition = et_new_definition.getText().toString();

        if ( (old_word.compareTo("") == 0) || (new_word.compareTo("") == 0) || (new_definition.compareTo("") == 0)){
            Toast.makeText(this,"Please fill all the 3 fields",Toast.LENGTH_SHORT).show();
            return;
        }

        // facciamo una query per ottenere l'id
        String selection = MyDictionaryDB.MyDictionaryEntry.COLUMN_NAME_WORD+" = ?";
        String[] arg_selection = {old_word+""};

        Cursor cursor = db.query(MyDictionaryDB.MyDictionaryEntry.TABLE_NAME,
                null,
                selection,
                arg_selection,
                null,
                null,
                null);
        if (cursor.getCount() == 0){
            Toast.makeText(this,"Replace failed, there are no word "+old_word+" inside the db",Toast.LENGTH_SHORT).show();
            return;
        }
        cursor.moveToFirst();
        String id = cursor.getString(0);
        cursor.close();

        // inserisco valore
        ContentValues values = new ContentValues();

        values.put(MyDictionaryDB.MyDictionaryEntry._ID, id);
        values.put(MyDictionaryDB.MyDictionaryEntry.COLUMN_NAME_DEF, new_definition);
        values.put(MyDictionaryDB.MyDictionaryEntry.COLUMN_NAME_WORD, new_word.toLowerCase().trim());

        db.replace(MyDictionaryDB.MyDictionaryEntry.TABLE_NAME,null,values);

        et_new_definition.setText("");
        et_new_word.setText("");
        et_old_word.setText("");

        Toast.makeText(this,"As requested the word is been replaced",Toast.LENGTH_SHORT).show();

    }


    public void show_dictionary(View view) {


        if (number_definition <= 0){
            Toast.makeText(this,"there are no word inside the db to show",Toast.LENGTH_SHORT).show();
            return;
        }

        if (timer.ismTimerRunning()){
            timer.pauseTimer();
            timer.azzeraTimer();
            correct_answer = 0;
            update_currentScore(correct_answer);
        }

        Intent intent = new Intent(this,ShowDictionaryActivity.class);
        startActivity(intent);
    }

    public void delete(View view) {
        EditText et_del_word = findViewById(R.id.word_del);
        String del_word = et_del_word.getText().toString().toLowerCase().trim();

        if (del_word.compareTo("") == 0){
            return;
        }

        String where = MyDictionaryDB.MyDictionaryEntry.COLUMN_NAME_WORD + " = ?";
        String[] where_arg = {del_word};

        // db.delete() ritorna il numero di righe cancellate
        if (db.delete(MyDictionaryDB.MyDictionaryEntry.TABLE_NAME, where, where_arg) > 0){
            Toast.makeText(this,"As requested the word "+del_word+" is been deleted",Toast.LENGTH_SHORT).show();
            number_definition--;
        }else{
            Toast.makeText(this,"Delete failed, there are no word "+del_word+" inside the db",Toast.LENGTH_SHORT).show();
        }

    }

    // PARTE OPTION

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    public void launchSettings(MenuItem item){
        Intent settingIntent = new Intent();
        settingIntent.setClass(this,SettingActivity.class);
        startActivity(settingIntent);
    }
}
