package com.example.ale.englishexam;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;

public class Speaking extends AppCompatActivity {

    private Timer timer;
    private ParseJson json_parse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speaking);

        // Timer
        TextView tw_timer = findViewById(R.id.timer);
        Button btt_startPause = findViewById(R.id.startPause);
        long timeDuration = 15*60*1000; // 15 minuti
        timer = new Timer(timeDuration, tw_timer, btt_startPause,this);

        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey("nome_file")){
            String nome_file = extras.getString("nome_file");

            // async task per il parsing dei dati da file json
            ParseJsonAT parseJsonAT = new ParseJsonAT();
            parseJsonAT.execute(nome_file);
        }

    }

    // se si preme indietro fermo timer
    @Override
    public void onBackPressed() {
        if (timer.ismTimerRunning()){
            timer.pauseTimer();
        }
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        if (timer.ismTimerRunning()){
            timer.pauseTimer();
        }
        super.onPause();
    }


    // PARTE TIMER
    public void startPause(View view) {
        // se il tempo scorre
        if (timer.ismTimerRunning()) {
            timer.pauseTimer();
        } else {
            timer.startTimer();
        }

    }

    // RIEMPI PARTE 1

    private void insertText(int id, String question){
        TextView textView = findViewById(id);
        textView.setText(question);
    }

    private void RiempiDomandeParte1(ParseJson json_parse) {
        json_parse.parseParte1_speaking();

        String[][] domandeparte1 = json_parse.getDomandeParte1();

        insertText(R.id.twparte1_question1,domandeparte1[0][0]);
        insertText(R.id.twparte1_question2,domandeparte1[0][1]);
        insertText(R.id.twparte1_question3,domandeparte1[0][2]);
    }

    private void insertImg(int id, String name_img){
        ImageView imageView = findViewById(id);
        int id_img = Speaking.this.getResources().getIdentifier(name_img,"drawable",Speaking.this.getPackageName());

        imageView.setImageResource(id_img);
    }

    private void RiempiDomandeParte2(ParseJson json_parse) {
        json_parse.parseParte2_speaking();

        String[][] domandeparte2 = json_parse.getDomandeParte2();

        insertText(R.id.twparte2_question,domandeparte2[0][0]);

        insertImg(R.id.img1, domandeparte2[0][1]);
        insertImg(R.id.img2, domandeparte2[0][2]);


    }

    private void RiempiDomandeParte3(ParseJson json_parse) {
        json_parse.parseParte3_speaking();

        String[][] domandeparte3 = json_parse.getDomandeParte3();

        insertText(R.id.twparte3_question,domandeparte3[0][0]);

        insertImg(R.id.img3, domandeparte3[0][1]);
    }

    // Asyntask per parsing file json
    private class ParseJsonAT extends AsyncTask<String, Void, ParseJson> {

        @Override
        protected ParseJson doInBackground(String... s) {
            String nome_file = s[0];

            // faccio parsing del file json
            Log.d("PJDM","nome file: "+nome_file);
            int id = Speaking.this.getResources().getIdentifier(nome_file,"raw",Speaking.this.getPackageName());
            InputStream is = Speaking.this.getResources().openRawResource(id);
            //Parse(is);
            json_parse = new ParseJson(is);
            Log.d("PJDM","input stream SPEAKING.java: "+is);
            Log.d("PJDM","level SPEAKING.java: "+json_parse.getLevel());
            // "titolo" -> da cambiare!!
            String titolo = "Speaking " + json_parse.getLevel()+" - "+json_parse.getSeries();
            TextView tw = findViewById(R.id.textView3);
            tw.setText(titolo);

            // chiudo file
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return json_parse;
        }

        @Override
        protected void onPostExecute(ParseJson json) {
            // INSERISCO I DATI - PARTE1 nota: domandaParte1 = [domandax][domanda,s1,s2,s3]
            RiempiDomandeParte1(json_parse);

            // INSERISCO I DATI - PARTE2 nota: domandaParte3 = [domandax][domandax]
            RiempiDomandeParte2(json_parse);

            RiempiDomandeParte3(json_parse);

        }
    }

}
