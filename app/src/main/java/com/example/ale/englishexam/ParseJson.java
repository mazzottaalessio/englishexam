package com.example.ale.englishexam;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class ParseJson {
    //private String exam_tipe;
    private String level;
    private String audio;
    private String series;

    private JSONObject jo;

    private JSONArray part1;
    private JSONArray part2;
    private JSONArray part3;
    private JSONArray part4;

    private String title_first_part;
    private String title_second_part;
    private String title_third_part;
    private String title_fourth_part;


    private String[][] domandeParte1 = new String[12][5];
    private String[][] domandeParte2 = new String[12][3];
    private String[][] domandeParte3 = new String[15][2];
    private String[][] domandeParte4 = new String[8][4];

    private String[] risposteParte1 = new String[12];
    private String[] risposteParte2 = new String[12];
    private String[] risposteParte3 = new String[15];
    private String[] risposteParte4 = new String[8];

    // READING
    private String testo_first_part;
    private String testo_second_part;
    private String testo_third_part;
    private String[][] title_test_part3 = new String[4][2];

    public String getTesto_third_part() {
        return testo_third_part;
    }



    public String getTitle_first_part(){
        try {
            title_first_part = jo.getString("title_first_part");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return title_first_part;
    }

    public String getTitle_second_part(){
        try {
            title_second_part = jo.getString("title_second_part");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return title_second_part;
    }

    public String getTitle_third_part(){
        try {
            title_third_part = jo.getString("title_third_part");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return title_third_part;
    }

    public String getTitle_fourth_part(){
        try {
            title_fourth_part = jo.getString("title_fourth_part");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return title_fourth_part;
    }

    public String[][] getTitle_test_part3() {
        return title_test_part3;
    }

    public String getTesto_first_part() {
        return testo_first_part;
    }

    public String getTesto_second_part() {
        return testo_second_part;
    }

    public String getSeries() {
        return series;
    }

    public String[][] getDomandeParte1() {
        return domandeParte1;
    }

    public String[][] getDomandeParte2() {
        return domandeParte2;
    }

    public String[][] getDomandeParte3() {
        return domandeParte3;
    }

    public String[][] getDomandeParte4() {
        return domandeParte4;
    }

    public String[] getRisposteParte1() {
        return risposteParte1;
    }

    public String[] getRisposteParte2() {
        return risposteParte2;
    }

    public String[] getRisposteParte3() {
        return risposteParte3;
    }

    public String[] getRisposteParte4() {
        return risposteParte4;
    }

    public String getLevel() {
        return level;
    }

    public String getAudio() {
        try {
            audio = jo.getString("audio");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return audio;
    }

    public ParseJson(InputStream is){

        Log.d("PJDM","entrato in ParseJson");

        BufferedReader reader = new BufferedReader(new InputStreamReader(is));

        // leggiamo il json
        String json = "";
        String line = null;

        //String testo="";

        try {
            line = reader.readLine();

            while (line != null){
                json += line;
                line = reader.readLine();
            }

            // chiudo file
            reader.close();

            jo = new JSONObject(json);
            //exam_tipe = jo.getString("exam_type");
            level = jo.getString("level");
            series = jo.getString("series");

        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

    }

    /* LISTENING */
    // PARSING PARTE1
    public void parseParte1(){
        try {
            // leggo audio (non posso farlo nel costruttore generale)
            Log.d("PJDM","STO IN PARSE PARTE1:");

            audio = jo.getString("audio");
            this.part1 = jo.getJSONArray("first_part");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < part1.length(); i++ ){
            JSONObject jsonObject;
            try {
                jsonObject = part1.getJSONObject(i);
                this.domandeParte1[i][0] = jsonObject.getString("question");
                this.domandeParte1[i][1] = jsonObject.getString("choose1");
                this.domandeParte1[i][2] = jsonObject.getString("choose2");
                this.domandeParte1[i][3] = jsonObject.getString("choose3");

                this.risposteParte1[i] = jsonObject.getString("answer");
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    // PARSING PARTE2
    public void parseParte2(){
        try {
            this.part2 = jo.getJSONArray("second_part");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < part2.length(); i++ ){
            JSONObject jsonObject;
            try {
                jsonObject = part2.getJSONObject(i);
                this.domandeParte2[i][0] = jsonObject.getString("question1");
                //this.domandeParte2[i][1] = jsonObject.getString("question2");

                this.risposteParte2[i] = jsonObject.getString("answer");
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    // PARSING PARTE3
    public void parseParte3(){
        try {
            this.part3 = jo.getJSONArray("third_part");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < part3.length(); i++ ){
            JSONObject jsonObject;
            try {
                jsonObject = part3.getJSONObject(i);
                this.domandeParte3[i][0] = jsonObject.getString("question");

                this.risposteParte3[i] = jsonObject.getString("answer");
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    // PARSING PARTE4
    public void parseParte4(){
        try {
            this.part4 = jo.getJSONArray("fourth_part");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < part4.length(); i++ ){
            JSONObject jsonObject;
            try {
                jsonObject = part4.getJSONObject(i);
                this.domandeParte4[i][0] = jsonObject.getString("question");
                this.domandeParte4[i][1] = jsonObject.getString("choose1");
                this.domandeParte4[i][2] = jsonObject.getString("choose2");
                this.domandeParte4[i][3] = jsonObject.getString("choose3");

                this.risposteParte4[i] = jsonObject.getString("answer");
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    // READING

    public void parseParte1_reading(){
        try {
            testo_first_part = jo.getString("testo_first_part");
            this.part1 = jo.getJSONArray("first_part");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < part1.length(); i++ ){
            JSONObject jsonObject;
            try {
                jsonObject = part1.getJSONObject(i);
                this.domandeParte1[i][0] = jsonObject.getString("question");
                this.domandeParte1[i][1] = jsonObject.getString("choose1");
                this.domandeParte1[i][2] = jsonObject.getString("choose2");
                this.domandeParte1[i][3] = jsonObject.getString("choose3");
                this.domandeParte1[i][4] = jsonObject.getString("choose4");

                this.risposteParte1[i] = jsonObject.getString("answer");
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public void parseParte3_reading(){
        try {
            title_test_part3[0][0] = jo.getString("title_third_part_A");
            title_test_part3[0][1] = jo.getString("testo_third_part_A");

            title_test_part3[1][0] = jo.getString("title_third_part_B");
            title_test_part3[1][1] = jo.getString("testo_third_part_B");

            title_test_part3[2][0] = jo.getString("title_third_part_C");
            title_test_part3[2][1] = jo.getString("testo_third_part_C");

            title_test_part3[3][0] = jo.getString("title_third_part_D");
            title_test_part3[3][1] = jo.getString("testo_third_part_D");

            this.part3 = jo.getJSONArray("third_part");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < part3.length(); i++ ){
            JSONObject jsonObject;
            try {
                jsonObject = part3.getJSONObject(i);
                this.domandeParte3[i][0] = jsonObject.getString("question");

                this.risposteParte3[i] = jsonObject.getString("answer");
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public void parseParte2_reading(){
        try {
            testo_second_part = jo.getString("testo_second_part");
            this.part2 = jo.getJSONArray("second_part");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < part2.length(); i++ ){
            JSONObject jsonObject;
            try {
                jsonObject = part2.getJSONObject(i);
                this.domandeParte2[i][0] = jsonObject.getString("question1");

                this.risposteParte2[i] = jsonObject.getString("answer");
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    // USE OF ENGLISH

    public void parseParte1_useofenglish(){
        parseParte1_reading();
    }

    public void parseParte2_useofenglish(){
        parseParte2_reading();
    }

    public void parseParte3_useofenglish(){
        try {
            testo_third_part = jo.getString("testo_third_part");
            this.part3 = jo.getJSONArray("third_part");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < part3.length(); i++ ){
            JSONObject jsonObject;
            try {
                jsonObject = part3.getJSONObject(i);
                this.domandeParte3[i][0] = jsonObject.getString("question");

                this.risposteParte3[i] = jsonObject.getString("answer");
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public void parseParte4_useofenglish(){
        try {
            this.part4 = jo.getJSONArray("fourth_part");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < part4.length(); i++ ){
            JSONObject jsonObject;
            try {
                jsonObject = part4.getJSONObject(i);
                this.domandeParte4[i][0] = jsonObject.getString("question");
                this.domandeParte4[i][1] = jsonObject.getString("question2");

                this.risposteParte4[i] = jsonObject.getString("answer");
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    // WRITING

    public void parseParte1_writing(){
        try {
            this.part1 = jo.getJSONArray("first_part");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < part1.length(); i++ ){
            JSONObject jsonObject;
            try {
                jsonObject = part1.getJSONObject(i);
                this.domandeParte1[i][0] = jsonObject.getString("question1");
                this.domandeParte1[i][1] = jsonObject.getString("question2");
                this.domandeParte1[i][2] = jsonObject.getString("question3");
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public void parseParte2_writing(){
        try {
            this.part2 = jo.getJSONArray("second_part");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < part2.length(); i++ ){
            JSONObject jsonObject;
            try {
                jsonObject = part2.getJSONObject(i);
                this.domandeParte2[i][0] = jsonObject.getString("question1");
                this.domandeParte2[i][1] = jsonObject.getString("question2");
                this.domandeParte2[i][2] = jsonObject.getString("question3");
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    // SPEAKING

    public void parseParte1_speaking(){
       parseParte1_writing();
    }

    public void parseParte2_speaking(){
        try {
            this.part2 = jo.getJSONArray("second_part");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < part2.length(); i++ ){
            JSONObject jsonObject;
            try {
                jsonObject = part2.getJSONObject(i);
                this.domandeParte2[i][0] = jsonObject.getString("question1");
                this.domandeParte2[i][1] = jsonObject.getString("img1");
                this.domandeParte2[i][2] = jsonObject.getString("img2");
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }


    }

    public void parseParte3_speaking(){
        try {
            this.part3 = jo.getJSONArray("third_part");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < part3.length(); i++ ){
            JSONObject jsonObject;
            try {
                jsonObject = part3.getJSONObject(i);
                this.domandeParte3[i][0] = jsonObject.getString("question");
                this.domandeParte3[i][1] = jsonObject.getString("img");
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}
